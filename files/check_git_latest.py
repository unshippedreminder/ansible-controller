#!/usr/bin/env python3

'''
Git Update Checker Script

This script is supposed to be run by a systemd service by ExecCondition

It checks for updates in two local Git repositories.
It performs a 'git pull' operation in each directory and exits without error if either repository is updated.
If no changes are found in either repository, it exits with a status of 1, stopping the service from executing further.

Usage:
    ./git_update_checker.py <path_to_first_repo> <path_to_second_repo>

Arguments:
    path_to_first_repo (str): Path to the first local Git repository.
    path_to_second_repo (str): Path to the second local Git repository.

Exit Codes:
    - 0: Updates found in at least one repository.
    - 1: No updates found in either repository.
    - 255: Error occurred during execution.

Note:
    Ensure that the provided paths are valid directories containing Git repositories.

Author: Mostly ChatGPT
'''

import os
import sys
import subprocess

# Constants
NUM_ARGUMENTS = len(sys.argv[1:])
REPO_PATH_FIRST = sys.argv[1]
REPO_PATH_SECOND = sys.argv[2]

# Fail if script is run with more than 2 arguments
if NUM_ARGUMENTS != 2:
    sys.exit(255)

def pull_and_check_changes(repo_path):
    '''
    Takes a path to a git repository as an argument
    Then it pulls the latest version and checks if there was an update
    Returns True if the repo was updated and False if it was already up to date
    '''
    try:
        # Change directory to the repo path.
        os.chdir(repo_path)
    except FileNotFoundError:
        print(f"Error: Directory not found: {repo_path}")
        sys.exit(255)
    except NotADirectoryError:
        print(f"Error: {repo_path} is not a directory")
        sys.exit(255)
    except PermissionError:
        print(f"Error: Permission denied for {repo_path}")
        sys.exit(255)
    except OSError as error:
        print(f"Error: Unable to change directory to {repo_path}: {error}")
        sys.exit(255)

    try:
        pull_output = subprocess.check_output(['git', 'pull']).decode('utf-8')
    except subprocess.CalledProcessError as error:
        print(f"Error executing 'git pull' in {repo_path}: {error}")
        sys.exit(255)

    if "Already up to date." in pull_output:
        return False

    return True

# First Repository
PULL_OUTPUT_FIRST = pull_and_check_changes(REPO_PATH_FIRST)

# Second Repository:
PULL_OUTPUT_SECOND = pull_and_check_changes(REPO_PATH_SECOND)

# Are there changes in either repository?

if PULL_OUTPUT_FIRST or PULL_OUTPUT_SECOND:
    sys.exit(0)
else:
    sys.exit(1)
