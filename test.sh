#!/usr/bin/bash
if [ "$1" = "update" ]; then
  scp -r mars@10.10.10.55:/var/home/mars/Nextcloud/code/ansible-controller ~/code/
  scp -r mars@10.10.10.55:/var/home/mars/Nextcloud/code/ansible-fedora_iot ~/code/
  ansible-galaxy install -f -r ../ansible-controller/roles/requirements-devel.yml
elif [ "$1" = "run" ]; then
  ansible-playbook -i staging --vault-password-file "/var/opt/ansible/vaultpass.unsippedreminder.ansible_controller" site.yml
else
  scp -r mars@10.10.10.55:/var/home/mars/Nextcloud/code/ansible-controller ~/code/
  scp -r mars@10.10.10.55:/var/home/mars/Nextcloud/code/ansible-fedora_iot ~/code/
  ansible-galaxy install -f -r ../ansible-controller/roles/requirements-devel.yml
  ansible-playbook -i staging --vault-password-file "/var/opt/ansible/vaultpass.unsippedreminder.ansible_controller" site.yml
fi

# Oneliner:
# scp -r mars@10.10.10.55:/var/home/mars/Nextcloud/code/ansible-controller ~/code/ && scp -r mars@10.10.10.55:/var/home/mars/Nextcloud/code/ansible-fedora_iot ~/code/ && ansible-galaxy install -f -r ../ansible-controller/roles/requirements-devel.yml && ansible-playbook -i staging --vault-password-file "/var/opt/ansible/vaultpass.unsippedreminder.ansible_controller" site.yml
